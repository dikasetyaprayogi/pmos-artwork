<img width="700" src=/src/banner.png>

New to pmOS artwork? See our [simple and easy general style guide](/docs/).

<details>
  <summary>Attribution</summary>

1. src/pmos-grass.blend: CC-BY 3.0 Grass background by https://www.blendswap.com/user/Spelle
1. /wallpapers/2024 , /docs & /src/banner.svg: CC-BY-SA 3.0 release wallpapers, readme & some artwork by https://gitlab.com/dikasetyaprayogi

</details>
