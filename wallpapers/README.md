<img width="700" src=/wallpapers/2024/wppbanner.png>

These are various wallpapers to be used across postmarketOS releases.

https://wiki.postmarketos.org/wiki/Wallpapers contains a list of all released wallpapers.

For general guidelines and a wallpaper test template see [wallpaper-guide.svg](/wallpapers/2024/wallpaper-guide.svg).